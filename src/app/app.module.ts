import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {MatDialogModule,MatCardModule, MatIconModule, MatToolbarModule} from '@angular/material'
import { AppComponent } from './app.component';
import { DemoComponent } from './demo/demo.component';
import { ParentComponent } from './parent/parent.component';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations'

@NgModule({
  declarations: [
    AppComponent,
    DemoComponent,
    ParentComponent
  ],
  imports: [
    BrowserModule,
    MatDialogModule,
    MatCardModule,
    MatIconModule,
    MatToolbarModule,
    BrowserAnimationsModule
  ],
  providers: [],
  entryComponents:[DemoComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
