import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {

  private name: string;
  
  constructor(private dialogRef:MatDialogRef<DemoComponent>,
  @Inject(MAT_DIALOG_DATA) public data:any ) { 

  }

  onNoClick():void{
    this.dialogRef.close('name');
  }

  ngOnInit() {
  }

}
