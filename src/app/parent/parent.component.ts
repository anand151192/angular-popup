import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DemoComponent } from '../demo/demo.component';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  isPopupOpend =false;
  constructor(private dialog?: MatDialog) { }

  ngOnInit() {
  }
 private test:string;
  openDialog(){
    this.isPopupOpend=true;
    const dialogRef = this.dialog.open(DemoComponent, {
      data: 'Test'
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.isPopupOpend=false;
      this.test=result;
    });
  }
}
